from sys import exit

# Define variables
LEGAL_OPERATORS = ("+", "-", "*", "/")
result = None

# Prints info
def print_help():
    # List legal operators for user
    print("\nLegal operators: ", end="")
    for operator in LEGAL_OPERATORS: print(operator + " ", end="")
    # List options
    print("\nUse 'r' in equation to implement result from last calculation")
    print("Type 'help' to display these options")
    print("Type 'exit' to quit")

# Calculate function
def calculate(num1, num2, operator):

    if operator == "*":
        res = num1 * num2
        print(f"{num1}*{num2}={res}")
        return res

    if operator == "/":
        if num2 == 0:
            print("ERROR: Invalid equation syntax - can't divide by zero")
            exit()
        res = num1 / num2
        print(f"{num1}/{num2}={res}")
        return res

    elif operator == "+":
        res = num1 + num2
        print(f"{num1}+{num2}={res}")
        return res

    elif operator == "-":
        res = num1 - num2
        print(f"{num1}-{num2}={res}")
        return res

# Init print help
print_help()

# Loop of usage
while True:

    # Ask user for equation to solve
    equation = input("\nEquation: ").replace(" ", "")
    if equation == 'help':
        print_help()
        continue
    elif equation == 'exit':
        exit()

    # Check if equation structure is legal
    if len(equation) < 3:
        print("ERROR: Invalid equation syntax - equation should contain at least 2 numbers seperated by 1 operator")
        exit()
    if equation[0] in LEGAL_OPERATORS or equation[-1] in LEGAL_OPERATORS: # if equation starts or end with an operator
        print("\nERROR: Invalid equation syntax - equation can't start or end with an operator")
        exit()
    for index, char in enumerate(equation):
        if char in LEGAL_OPERATORS and equation[index + 1] in LEGAL_OPERATORS: # if equation contains 2 operators in a row
            print("\nERROR: Invalid equation syntax - equation contains 2 operators in a row")
            exit()
        elif index + 1 != len(equation):
            if char == "." and equation[index + 1] == ".":
                print("\nERROR: Invalid equation syntax - equation contains 2 commas in a row")
                exit()
    for char in equation:
        if char in LEGAL_OPERATORS:
            contains_operator = True
            break
        else: contains_operator = False
    if contains_operator is True: pass
    else:
        print("\nERROR: Invalid equation syntax - equation must contain at least one operator")
        exit()

    for index, char in enumerate(equation):
        if char == "r": # if result from former calculation is available - 'r' is a valid value
            if result is not None:
                print(f"'r' = {result}")
                equation = equation[0:index] + str(result) + equation[index:len(equation)]
                equation = equation.replace("r", "")
                print("Final equation:", equation)
            else:
                print("ERROR: Invalid equation syntax - you must complete at least one calculation before using 'r'")
                exit()
    
    # Check if all chars in equation is legal and some special structure check
    count_commas = 0
    for index, char in enumerate(equation):
        if char.isdigit(): pass # check if char is num
        elif char == ".":
            count_commas += 1 # count numbers of commas in the current number section of the equation
        elif char in LEGAL_OPERATORS:
            if count_commas > 1: # check if float number contains more than 1 comma
                print("ERROR: Invalid equation syntax - too many commas in float")
                exit()
            else:
                count_commas = 0
        else: # it must be an illegal char then
            print(f"\nERROR: Invalid equation syntax - couldn't handle char '{char}'")
            exit()

        if index + 1 == len(equation) and count_commas > 1: # end of equation - check if last number had too many commas
            print("ERROR: Invalid equation syntax - too many commas in float")
            exit()

    # Sort equation into a processable format
    num_grouping = None
    sorted_equation = []
    for index, char in enumerate(equation):
        if char.isdigit() or char == ".": # check if char can be converted to int or char is part of a float
            if num_grouping is None: num_grouping = char
            else: num_grouping += char
        elif char in LEGAL_OPERATORS: # char is operator
            if "." in num_grouping: sorted_equation.append(float(num_grouping))
            else: sorted_equation.append(int(num_grouping))
            num_grouping = None
            sorted_equation.append(char)
        if index + 1 == len(equation): # end of equation - append last num group
                if "." in num_grouping: sorted_equation.append(float(num_grouping))
                else: sorted_equation.append(int(num_grouping))

    # Solve equation
    print("")

    # Multiplication
    for index, element in enumerate(sorted_equation):
        if element == '*':
            sorted_equation[index - 1] = calculate(sorted_equation[index - 1],
                                                   sorted_equation[index + 1],
                                                   element)
            sorted_equation.pop(index + 1) # Remove multiply operator and num2 from list
            sorted_equation.pop(index)
        elif element == '/':
            sorted_equation[index - 1] = calculate(sorted_equation[index - 1],
                                                   sorted_equation[index + 1],
                                                   element)
            sorted_equation.pop(index + 1) # Remove divide operator and num2 from list
            sorted_equation.pop(index)

    # Addition and subtraction
    if len(sorted_equation) == 1: # check if multiplication sorted the whole equation
        result = sorted_equation[0]
        if isinstance(result, float) and result.is_integer(): result = int(result) # if result is a float and can be converted to an int
    else: # else start addition and subtraction
        temp_result = None
        for index, element in enumerate(sorted_equation):
            if element in LEGAL_OPERATORS:
                if temp_result is None:
                    temp_result = calculate(sorted_equation[index - 1], 
                                            sorted_equation[index + 1],
                                            element)
                else:
                    temp_result = calculate(temp_result, 
                                            sorted_equation[index + 1],
                                            element)
        result = temp_result
        if isinstance(result, float) and result.is_integer(): result = int(result)

    print("Result={}".format(result))